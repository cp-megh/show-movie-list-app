package com.example.practical51

import com.example.practical51.api.ApiService
import com.example.practical51.model.Movie
import com.example.practical51.utils.AppDispatcher
import io.reactivex.rxjava3.core.Observable
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.doSuspendableAnswer
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

class MoviesViewModelTest {
    @ExperimentalCoroutinesApi
    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    private lateinit var viewModel: MoviesViewModel
    private val service = mock<ApiService>()
    private val appDispatcher = mock<AppDispatcher>()

    companion object {
        private val movie = Movie(
            "Dawn of the Planet of the Apes",
            "https://api.androidhive.info/json/movies/1.jpg",
            8.3,
            2014,
            listOf("Action", "Drama", "Sci-Fi")
        )
    }

    @Before
    fun setup() {
        initViewModel()
    }

    private fun initViewModel() {
        viewModel =
            MoviesViewModel(
                service,
                appDispatcher
            )
    }

    @Test
    fun test_resetState_whenNetwork_Found() = runBlocking {
        val startState = flowOf(MovieState.START)
        viewModel.resetState()
        Assert.assertEquals(startState.first(), viewModel.state.value)
    }

    @Test
    fun test_getUsersList() = runBlocking {
        val loadingData = flowOf(MovieState.LOADING)
        whenever(service.getMovieList()).doSuspendableAnswer {
            withContext(Dispatchers.IO) { delay(5000) }
            Observable.just(emptyList())
        }
        initViewModel()
        viewModel.getMoviesList()
        Assert.assertEquals(loadingData.first(), viewModel.state.value)
        whenever(service.getMovieList()).thenReturn(Observable.just(listOf(movie)))
        initViewModel()
        viewModel.getMoviesList()
        val successState =
            flowOf(MovieState.SUCCESS(listOf(movie)))
        Assert.assertEquals(successState.first(), viewModel.state.value)
    }

}