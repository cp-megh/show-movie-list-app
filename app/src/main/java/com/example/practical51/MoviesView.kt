package com.example.practical51

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import coil.compose.rememberImagePainter
import coil.transform.CircleCropTransformation
import com.example.practical51.utils.showBanner
import com.example.practical51.model.Movie
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ViewMoviesActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MaterialTheme {
                Surface(color = MaterialTheme.colors.background) {
                    MovieList()
                }
            }
        }
    }
}

@Composable
fun MovieList() {
    val viewModel = hiltViewModel<MoviesViewModel>()
    val state by viewModel.state.collectAsState()
    val context = LocalContext.current
    state.let { state ->
        when (state) {
            MovieState.START -> {}
            MovieState.LOADING -> {
                Box(
                    contentAlignment = Alignment.Center,
                    modifier = Modifier.fillMaxSize()
                ) {
                    CircularProgressIndicator(color = colorResource(id = R.color.teal_700))
                }
            }
            is MovieState.SUCCESS -> {
                val movieList = state.movieList
                LazyColumn(contentPadding = PaddingValues(16.dp)) {
                    itemsIndexed(movieList) { _, item ->
                        MovieView(movie = item)
                    }
                }
            }
            is MovieState.FAILURE -> {
                val message = state.failureMessage
                LaunchedEffect(key1 = message) {
                    showBanner(context, message)
                    viewModel.resetState()
                }
            }
        }

    }

}

@Composable
fun MovieView(movie: Movie) {
    Card(
        modifier = Modifier
            .padding(8.dp, 4.dp)
            .padding(4.dp)
            .fillMaxSize()
            .clickable { },
        shape = RoundedCornerShape(8.dp),
        elevation = 4.dp,
    ) {
        Row(
            Modifier
                .fillMaxWidth()
        ) {
            Image(
                painter = rememberImagePainter(data = movie.image,
                    builder = {
                        transformations(CircleCropTransformation())
                        crossfade(1000)
                    }),
                contentDescription = stringResource(R.string.movie_poster_description),
                modifier = Modifier
                    .size(80.dp)
                    .weight(1f)
                    .padding(start = 4.dp)
            )
            Column(
                verticalArrangement = Arrangement.Center,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(start = 8.dp)
                    .weight(3f)
            ) {
                Text(
                    text = movie.title,
                    style = MaterialTheme.typography.subtitle1,
                    fontWeight = FontWeight.Bold,
                    maxLines = 1,
                    overflow = TextOverflow.Ellipsis,
                )
                Row(
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Icon(
                        painter = painterResource(id = R.drawable.ic_baseline_star_24),
                        contentDescription = stringResource(R.string.rating_image_description),
                        tint = colorResource(id = R.color.star)
                    )
                    Text(
                        text = """ ${movie.rating}/10""",
                        style = MaterialTheme.typography.subtitle1,
                        fontWeight = FontWeight.Bold
                    )
                }

                Row(
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Icon(
                        painter = painterResource(id = R.drawable.ic_baseline_calendar_month_24),
                        contentDescription = stringResource(R.string.rating_image_description),
                    )
                    Text(
                        text = movie.releaseYear.toString(),
                        style = MaterialTheme.typography.subtitle1,
                        modifier = Modifier.padding(start = 4.dp),
                        fontWeight = FontWeight.Bold
                    )
                }
            }
        }
    }
}