package com.example.practical51.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Movie(

    @SerializedName("title")
    @Expose
    val title: String,

    @SerializedName("image")
    @Expose
    val image: String,

    @SerializedName("rating")
    @Expose
    val rating: Double,

    @SerializedName("releaseYear")
    @Expose
    val releaseYear: Int,

    @SerializedName("genre")
    @Expose
    val genre: List<String>

)