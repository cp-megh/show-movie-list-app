package com.example.practical51.api

import com.example.practical51.model.Movie
import io.reactivex.rxjava3.core.Observable
import retrofit2.http.GET

interface ApiService {
    @GET("json/movies.json")
    fun getMovieList(): Observable<List<Movie>>
}