package com.example.practical51

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.practical51.api.ApiService
import com.example.practical51.model.Movie
import com.example.practical51.utils.AppDispatcher
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observer
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

@HiltViewModel
class MoviesViewModel @Inject constructor(
    private val service: ApiService,
    private val appDispatcher: AppDispatcher
) : ViewModel() {
    val state = MutableStateFlow<MovieState>(MovieState.START)

    init {
        getMoviesList()
    }

    fun getMoviesList() {
        viewModelScope.launch {
            state.value = MovieState.LOADING
            withContext(appDispatcher.IO) {
                service.getMovieList()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(object : Observer<List<Movie>> {
                        override fun onSubscribe(d: Disposable) {}
                        override fun onNext(movieList: List<Movie>) {
                            state.value = MovieState.SUCCESS(
                                movieList = movieList
                            )
                        }

                        override fun onError(e: Throwable) {
                            state.value = MovieState.FAILURE(
                                failureMessage = e.localizedMessage!!
                            )
                        }

                        override fun onComplete() {}
                    })
            }
        }
    }

    fun resetState() {
        state.value = MovieState.START
    }
}

sealed class MovieState {
    object START : MovieState()
    object LOADING : MovieState()
    data class SUCCESS(
        val movieList: List<Movie>
    ) : MovieState()

    data class FAILURE(val failureMessage: String) : MovieState()
}